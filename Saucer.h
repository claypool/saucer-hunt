//
// Saucer.h
//
#ifndef SAUCER_H
#define SAUCER_H

#include "Object.h"
 
class Saucer : public Object {
 
 private:
  void moveToStart();
  void adjustSpeed();
  int move_countdown;

 public:
  Saucer();
  ~Saucer();
  int eventHandler(Event *p_e);
};

#endif
