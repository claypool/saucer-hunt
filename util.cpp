//
// utility.cpp
//

// System includes.
#include <math.h>

// Engine includes.
#include "WorldManager.h"

// Game includes
#include "game.h"
#include "Queue.h"
#include "Saucer.h"
#include "utility.h"

// Return pointer to Saucer, NULL if not found.
Saucer *getSaucer() {
  ObjectList object_list = WM.getAllObjects();
  ObjectListIterator oi(&object_list);
  Object *p_o;
  for (oi.first(); !oi.isDone(); oi.next()) {
    p_o = oi.currentObject();
    if (p_o -> getType() == "Saucer")
      break;
  }
  if (oi.isDone()) {
    LM.writeLog("getSaucer(): Error! Saucer not found.");
    return NULL;
  }

  return static_cast <Saucer *> (p_o);
}

// Return pointer to Queue, NULL if not found.
Queue *getQueue() {
  ObjectList object_list = WM.getAllObjects();
  ObjectListIterator oi(&object_list);
  Object *p_o;
  for (oi.first(); !oi.isDone(); oi.next()) {
    p_o = oi.currentObject();
    if (p_o -> getType() == "Queue")
      break;
  }
  if (oi.isDone()) {
    LM.writeLog("getQueue(): Error! Queue not found.");
    return NULL;
  }

  return static_cast <Queue *> (p_o);
}

///////////////////////////////////////////////////////////
// Option A: straight lookup from experimental data.
float EIlookup(int lag, int damage, float speed, int aoe) {

  // Open stats file.
  char filename[80];
  if (speed == (int) speed)
    sprintf(filename, "expected-impact/speed-%.0f-aoe-%d.data", speed, aoe);
  else
    sprintf(filename, "expected-impact/speed-%.2f-aoe-%d.data", speed, aoe);
  FILE *p_f = fopen(filename, "r");
  if (!p_f) {
    LM.writeLog("computeExpectedImpact(): Error! Unable to open file '%s'.", 
		filename);
    return 0.0;
  }

  // Expect data in the form "lag value", one per row.
  // Search for "lag" value.
  int lag_data;
  float impact_data;
  LM.writeLog(10, "EIlookup():");
  while (!feof(p_f)) {
    int ret = fscanf(p_f, "%d %f %*[^\n]", &lag_data, &impact_data);
    LM.writeLog(10, "\t%d\t%.2f", lag_data, impact_data);
    if (ret != 2 && !feof(p_f)) {
      LM.writeLog("computeExpectedImpact(): Error! Expect \"lag value\" each row.");
      fclose(p_f);
      return 0.0; // error
    } 
    if (lag_data == lag)  // lag match
      break;
  }
  if (lag_data != lag) {  // if done with loop and didn't find lag ...
    LM.writeLog("computeExpectedImpact(): Error! Lag value %d not found.", 
		lag);
    fclose(p_f);
    return 0.0; // error
  }

  // Close file.
  fclose(p_f);

  // Impact is impact_data * damage.
  float impact = impact_data * damage;
  return impact;
}

///////////////////////////////////////////////////////////
// Option B: linear model based on experimental data.
float EIlinear(int lag, int damage, float speed, int aoe) { 

  // Open linear regression data file.
  char filename[80];
  sprintf(filename, "expected-impact/linear.data");
  FILE *p_f = fopen(filename, "r");
  if (!p_f) {
    LM.writeLog("EIlinear(): Error! Unable to open file '%s'.", 
		filename);
    return 0.0;
  }
  // discard first line of text values
  int ret = fscanf(p_f, "%*[^\n]");

  // Expect data in the form 'speed aoe slope y-intercept',
  // one per row.  Search for right "speed aoe" combination.
  float speed_in, slope_in, y_in;
  int aoe_in;
  LM.writeLog(10, "EIlinear():");
  while (!feof(p_f)) {
    ret = fscanf(p_f, "%f %d %f %f %*[^\n]", 
		 &speed_in, &aoe_in, &slope_in, &y_in);
    LM.writeLog(15, "\treturn %d", ret);
    LM.writeLog(10, "\t%.2f %d %.2f %.2f", speed_in, aoe_in, slope_in, y_in);
    if (ret != 4 && !feof(p_f)) {
      LM.writeLog("EIlinear(): Error! Expect \"speed aoe slope y-intercept\" each row.");
      fclose(p_f);
      return 0.0; // error
    } 
    if (speed_in == speed && aoe_in == aoe) // speed & aoe match
      break;
  }
  if (speed_in != speed || aoe_in != aoe) { // speed & aoe non-match
    LM.writeLog("EIlinear(): Error! Speed %.2f and AoE %d not found.", 
		speed, aoe);
    fclose(p_f);
    return 0.0; // error
  }

  // Close file.
  fclose(p_f);

  // Impact = y-value of line at the x=lag, multiplied by damage.
  float impact = (slope_in * lag + y_in) * damage;
  return impact;
}

///////////////////////////////////////////////////////////
// Option C: exponential model based on experimental data.
float EIexponential(int lag, int damage, float speed, int aoe) { 

  // Open exponential model data file.
  char filename[80];
  sprintf(filename, "expected-impact/exponential.data");
  FILE *p_f = fopen(filename, "r");
  if (!p_f) {
    LM.writeLog("EIexponential(): Error! Unable to open file '%s'.", 
		filename);
    return 0.0;
  }
  // discard first line of text values
  int ret = fscanf(p_f, "%*[^\n]");

  // Equation is: ei = a * exp(b*x)
  // Expect data in the form 'speed aoe a b',
  // one per row.  Search for right "speed aoe" combination.
  float speed_in, a_in, b_in;
  int aoe_in;
  LM.writeLog(10, "EIexponential():");
  while (!feof(p_f)) {
    ret = fscanf(p_f, "%f %d %f %f %*[^\n]", &speed_in, &aoe_in, &a_in, &b_in);
    LM.writeLog(15, "\treturn %d", ret);
    LM.writeLog(10, "\t%.2f %d %.2f %.2f", speed_in, aoe_in, a_in, b_in);
    if (ret != 4 && !feof(p_f)) {
      LM.writeLog("EIexponential(): Error! Expect \"speed aoe a b\" each row.");
      fclose(p_f);
      return 0.0; // error
    } 
    if (speed_in == speed && aoe_in == aoe) // speed & aoe match
      break;
  }
  if (speed_in != speed || aoe_in != aoe) { // speed & aoe non-match
    LM.writeLog("EIexponential(): Error! Speed %.2f and AoE %d not found.", 
		speed, aoe);
    fclose(p_f);
    return 0.0; // error
  }

  // Close file.
  fclose(p_f);

  // Impact = a * exp(b*x), multiplied by damage.
  float impact = (a_in * exp(b_in*lag)) * damage;
  return impact;
}

// Compute expected impact at given lag based on Damage, Speed and AoE.
float computeExpectedImpact(int lag, int damage, float speed, int aoe) {

#ifdef EI_LOOKUP
  // Option A: straight lookup from experimental data.
  return EIlookup(lag, damage, speed, aoe);

#elif EI_LINEAR
  // Option B: linear model based on experimental data.
  return EIlinear(lag, damage, speed, aoe);

#elif EI_EXPONENTIAL
  // Option C: exponential model based on experimental data.
  return EIexponential(lag, damage, speed, aoe);

#else
  return 0.0;
#endif
}

