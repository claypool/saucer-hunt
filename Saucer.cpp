//
// Saucer.cpp
//

#include <stdlib.h> 		// for random()

// Engine includes.
#include "EventCollision.h"
#include "EventOut.h"
#include "EventStep.h"
#include "GameManager.h"
#include "LogManager.h"
#include "ResourceManager.h"
#include "WorldManager.h"
 
// Game includes.
#include "Config.h"
#include "Explosion.h"
#include "game.h"
#include "Saucer.h"

Saucer::Saucer() {

#ifndef HEADLESS
  // Setup "saucer" sprite.
  Sprite *p_temp_sprite = RM.getSprite("saucer");
  if (!p_temp_sprite) {
    LM.writeLog("Saucer::Saucer(): Warning! Sprite 'saucer' not found");
  } else {
    setSprite(p_temp_sprite);
    setSpriteSlowdown(8);		
  }
#endif

  // Set object properties.
  setType("Saucer");
  setAltitude(3);
  
  // Move Saucer to starting position.
  moveToStart();

  // Step events
  registerInterest(DF_STEP_EVENT);

  move_countdown = 40;
}

Saucer::~Saucer() {

  // If game over, don't do anything.
  if (GaM.getGameOver())
    return;

  // Create an explosion.
  Explosion *p_explosion = new Explosion;
  p_explosion -> setPosition(this -> getPosition());
  
  // Saucers apper stay around perpetually.
  new Saucer();
}

// Handle event.
// Return 0 if ignored, else 1.
int Saucer::eventHandler(Event *p_e) {

  if (p_e->getType() == DF_OUT_EVENT) {
    moveToStart();
    return 1;
  }

  if (p_e->getType() == DF_STEP_EVENT) {
    move_countdown--;
    if (move_countdown <= 0) {
      move_countdown = 40;
      adjustSpeed();
    }
    return 1;
  }

  // If get here, have ignored this event.
  return 0;
}

// Adjust speed for Saucer.
// 50% of time, move at average speed.
// 25% of time, move at 0.5 speed.
// 25% of time, move at 1.5 speed. 
void Saucer::adjustSpeed() {

  // Get direction.
  int direction;
  if (getXVelocity() < 0) 
    direction = -1;
  else
    direction = 1;

  // Roll dice for speed.
  float speed = Config::getInstance()->getSpeedSaucer();
#ifndef CONSTANT_SPEED
  int dice = random()%12;
  switch (dice) {
  case 0: speed /= 4; break;
  case 1: speed /= 3; break;
  case 2: speed /= 2; break;
  case 3: speed *= 2; break;
  case 4: speed *= 3; break;
  case 5: speed *= 4; break;
  default: speed = speed; break;
  }
#endif

  // Set speed.
  setXVelocity(speed * direction);
}

// Move Saucer to starting location.
void Saucer::moveToStart() {

  // Increment id to act as if this is a new Saucer.
  setId(getId()+1);

  // y is at top, below points.
  Position temp_pos;
  temp_pos.setY(4);

  // x is set to left side or right side
  int side;
  if (random()%2) {
    temp_pos.setX(Config::getInstance()->getWorldHorizontal() + 3);
    setXVelocity(-1);
    side = -1;
  } else {
    temp_pos.setX(-3);
    setXVelocity(1);
    side = 1;
  }

  // Adjust speed.
  adjustSpeed();
  LM.writeLog(5, "Saucer::moveToStart(): x velocity %.1f", getXVelocity());

  // If collision, move over slightly until empty space.
  ObjectList collision_list = WM.isCollision(this, temp_pos);
  while (!collision_list.isEmpty()) {
    temp_pos.setX(temp_pos.getX()+side);
    collision_list = WM.isCollision(this, temp_pos);
  }

  // Move Saucer to new location.
  WM.moveObject(this, temp_pos);
}

