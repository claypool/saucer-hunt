#include "Stats.h"

Stats::Stats() {
  for (int i=0; i<MAX_WEAPONS; i++) {
    bullets_fired[i] = 0;
    bullets_hit[i] = 0;
    bullets_points[i] = 0;
  }
  points = 0;
}

// Return singleton instance.
Stats *Stats::getInstance() {
  static Stats stats;
  return &stats;
}

// Set bullets fired.
void Stats::setBulletsFired(int gun, int bullets_fired) {
  this->bullets_fired[gun] = bullets_fired;
}

// Increment bullets fired.
void Stats::incBulletsFired(int gun) {
  bullets_fired[gun]++;
}

// Return bullets fired.
int Stats::getBulletsFired(int gun) const {
  return bullets_fired[gun];
}

// Set bullets hit.
void Stats::setBulletsHit(int gun, int bullets_hit) {
  this->bullets_hit[gun] = bullets_hit;
}

// Increment bullets hit.
void Stats::incBulletsHit(int gun) {
  bullets_hit[gun]++;
}

// Return bullets hit.
int Stats::getBulletsHit(int gun) const {
  return bullets_hit[gun];
}

// Set points.
void Stats::setPoints(int gun, int points) {
  this->bullets_points[gun] = points;
}

// Increment points by delta.
void Stats::incPoints(int gun, int delta) {
  bullets_points[gun] += delta;
  points += delta;
}

// Get points.
int Stats::getPoints(int gun) const {
  return bullets_points[gun];
}

// Get points.
int Stats::getTotalPoints() const {
  return points;
}

