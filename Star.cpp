//
// Star.cpp
//

#include <stdlib.h>		// for random

// Engine includes.
#include "EventStep.h"
#include "GraphicsManager.h"
#include "WorldManager.h"

#define STAR_LIFE 900
#define STAR_DEATH 30

// Game includes.
#include "Star.h"

Star::Star() {
  setType("Star");
  setSolidness(SPECTRAL);
  setAltitude(0);	// Make them in the background.
  WorldManager &world_manager = WorldManager::getInstance();
  Position pos(random()%world_manager.getBoundary().getHorizontal(),
	       random()%world_manager.getBoundary().getVertical());
  setPosition(pos);
  count = random()%STAR_LIFE;		// Random time to live.
  registerInterest(DF_STEP_EVENT);
}

void Star::birth() {
  count = random()%STAR_LIFE;	
  WorldManager &world_manager = WorldManager::getInstance();
  Position pos(random()%world_manager.getBoundary().getHorizontal(),
	       random()%world_manager.getBoundary().getVertical());
  setPosition(pos);
}

// Draw stars if not "dead" and not "twinkling".
void Star::draw() {
  if (count > 0 && random()%100 > 0)  {
    GraphicsManager &graphics_manager = GraphicsManager::getInstance();
    graphics_manager.drawCh(getPosition(), STAR_CHAR, COLOR_WHITE);
  }
}

// Handle event.
// Return 0 if ignored, else 1.
int Star::eventHandler(Event *p_e) {

  if (p_e->getType() == DF_STEP_EVENT) {
    count--;
    if (count < (-1*random()%STAR_DEATH))
      birth();
  }

  // If get here, have ignored this event.
  return 0;
}

