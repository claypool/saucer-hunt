//
// TestQueue.cpp
//

/* 
FCFS/FIFO queue should be:
0: time 20, enq_time 10, impact_time 10, weapon 1, impact 0.75
1: time 25, enq_time 11, impact_time 14, weapon 2, impact 0.69
2: time 30, enq_time 12, impact_time 18, weapon 3, impact 1.12
3: time 35, enq_time 13, impact_time 22, weapon 4, impact 0.11
TOTAL: 2.67

LOOKUP priority queue should be:
0: time 20, enq_time 10, impact_time 10, weapon 1, impact 0.75
1: time 25, enq_time 13, impact_time 12, weapon 4, impact 0.53
2: time 30, enq_time 11, impact_time 19, weapon 2, impact 0.69
3: time 35, enq_time 12, impact_time 23, weapon 3, impact 1.02
TOTAL: 2.99

LINEAR priority queue should be:
0: time 20, enq_time 10, impact_time 10, weapon 1, impact 0.75
1: time 25, enq_time 13, impact_time 12, weapon 4, impact 0.46
2: time 30, enq_time 12, impact_time 18, weapon 3, impact 1.11
3: time 35, enq_time 11, impact_time 24, weapon 2, impact 0.57
TOTAL 2.89

EXP priority queue should be:
0: time 20, enq_time 10, impact_time 10, weapon 1, impact 0.75
1: time 25, enq_time 13, impact_time 12, weapon 4, impact 0.38
2: time 30, enq_time 12, impact_time 18, weapon 3, impact 1.10
3: time 35, enq_time 11, impact_time 24, weapon 2, impact 0.57
TOTAL 2.80
*/

// Engine includes.
#include "EventStep.h"
#include "GameManager.h"

// Game includes.
#include "TestQueue.h"
#include "Config.h"

TestQueue::TestQueue() {

  Config::getInstance() -> setLag(10);
  Config::getInstance() -> setGap(5);

  // linear: a*-0.012854483871 + 0.881258870968
  // linear at 10: 0.75
  // lookup at 10: expected is 0.75
  w1.setId(1);
  w1.setDamage(1);
  w1.setAoE(10);
  w1.setSpeed(1); 

  // linear: a*-0.012854483871 + 0.881258870968
  // linear at 10: 0.75
  // linear at 14: 0.70
  // linear at 19: 0.64
  // linear at 24: 0.57
  // lookup at 10: expected is 0.75
  // lookup at 14: expected is 0.69
  // lookup at 19: expected is 0.62
  w2.setId(2);
  w2.setDamage(1);
  w2.setSpeed(1);
  w2.setAoE(10);

  // linear: a*-0.007305604839 + 0.466148588710
  // linear at 13: 0.37 * 3 = 1.11
  // linear at 18: 0.33 * 3 = 1.00
  // lookup at 13: expected is 0.390030 * 3 = 1.17
  // lookup at 18: expected is 0.373000 * 3 = 1.12
  w3.setId(3);
  w3.setDamage(3);
  w3.setSpeed(0.25);
  w3.setAoE(5);

  // linear: a*-0.029387838710 + 0.810217903226
  // linear at 12: 0.46
  // linear at 17: 0.31
  // linear at 22: 0.16
  // lookup at 12: expected is 0.53
  // lookup at 17: expected is 0.13
  // lookup at 22: expected is 0.11
  w4.setId(4);
  w4.setDamage(1);
  w4.setSpeed(4);
  w4.setAoE(3);

  p_q = new Queue;

  registerInterest(DF_STEP_EVENT);
}

int TestQueue::eventHandler(Event *p_e) {

  if (p_e->getType() == DF_STEP_EVENT) {
    EventStep *p_se = static_cast <EventStep *> (p_e);

    LM.writeLog("TestQueue::eventHandler(): step count is %d",
		p_se->getStepCount());

    if (p_se->getStepCount() == 10) {
      // Add first node.  
      Bullet *p_b1 = new Bullet(Position(0,0), w1);
      Node n1(p_b1);
      p_q -> enqueue(n1);
      p_q -> printQueue();
    }

    if (p_se->getStepCount() == 11) {
      // Add second node. 
      Bullet *p_b2 = new Bullet(Position(0,0), w2);
      Node n2(p_b2);
      p_q -> enqueue(n2);
      p_q -> printQueue();
    }

    if (p_se->getStepCount() == 12) {
      // Add third node.  
      Bullet *p_b3 = new Bullet(Position(0,0), w3);
      Node n3(p_b3);
      p_q -> enqueue(n3);
      p_q -> printQueue();
    }

    if (p_se->getStepCount() == 13) {
      // Add fourth node. 
      Bullet *p_b4 = new Bullet(Position(0,0), w4);
      Node n4(p_b4);
      p_q -> enqueue(n4);
      p_q -> printQueue();
    }

    if (p_se->getStepCount() == 14)
      GaM.setGameOver();
    
    return 1;
  }

  return 0;
}
