//
// Config.h - singleton to keep global parameters
//

#ifndef CONFIG_H
#define CONFIG_H

// Game includes.
#include "game.h"
#include "Weapon.h"

class Config {

 private:
  Config();			 // Private since a singleton.
  Config (Config const&);	 // Don't allow copy.
  void operator=(Config const&); // Don't allow assignment.
  int lag;			 // Lag between actions (in frames).
  int world_horizontal;	         // Horizontal width of world (in spaces).
  int world_vertical;		 // Vertical height of world (in spaces).
  float speed_saucer;		 // Saucer speed (in slowdown).
  int frame_time;		 // Time between game loop steps (in ms).
  int game_time;		 // Length game should run (in frames).
  int gap;			 // Min gap between weapon shots (in frames).
  int weapon_count;		 // Number of different weapons in game.
  Weapon weapon[MAX_WEAPONS];	 // Weapons in game.

 public:
  static Config *getInstance();

  // Get attributes.
  int getLag() const;
  int getWorldHorizontal() const;	 
  int getWorldVertical() const;		 
  float getSpeedSaucer() const;		 
  int getFrameTime() const;		 
  int getGameTime() const;		 
  int getGap() const;		 
  int getWeaponCount() const;		 
  Weapon getWeapon(int gun) const;		 

  // Set attributes
  void setLag(int lag);
  void setWorldHorizontal(int world_horizontal);	 
  void setWorldVertical(int world_vertical);		 
  void setSpeedSaucer(float speed_saucer);		 
  void setFrameTime(int frame_time);		 
  void setGameTime(int game_time);		 
  void setGap(int gap);		 
  void setWeaponCount(int weapon_count);
  void addWeapon(Weapon weapon, int gun);
};

#endif
