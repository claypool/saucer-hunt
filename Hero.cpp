//
// Hero.cpp
//

#include <stdlib.h>		// for abs()

// Engine includes.
#ifdef NO_AI
#include "EventKeyboard.h"
#endif
#include "EventStep.h"
#include "EventView.h"
#include "GameManager.h"
#include "GraphicsManager.h"
#include "LogManager.h"
#include "ResourceManager.h"
#include "WorldManager.h"

// Game includes.
#include "Bullet.h"
#include "Config.h"
#include "Explosion.h"
#include "game.h"
#include "Hero.h"
#include "Saucer.h"
#include "Stats.h"
#include "util.h"

Hero::Hero() {

#ifndef HEADLESS
  // Link to "ship" sprite.
  Sprite *p_temp_sprite;
  p_temp_sprite = RM.getSprite("ship");
  if (!p_temp_sprite) {
    LM.writeLog("Hero::Hero(): Warning! Sprite '%s' not found", "ship");
  } else {
    setSprite(p_temp_sprite);
    setSpriteSlowdown(3);	// 1/3 speed animation.
    setTransparency();	        // Transparent sprite.
  }
#endif

  // Need to update fire rate control each step.
  registerInterest(DF_STEP_EVENT);

  // Set object type.
  setType("Hero");

  // Set starting location.
  Position pos(Config::getInstance()->getWorldHorizontal()/2, 
	       Config::getInstance()->getWorldVertical()-2);
  setPosition(pos);

  lag_countdown = 0;
  weapon_count = 0;

#ifdef NO_AI
  // If player can control, need keyboard.
  registerInterest(DF_KEYBOARD_EVENT);
#endif
}

// Handle event.
// Return 0 if ignored, else 1.
int Hero::eventHandler(Event *p_e) {

  if (p_e->getType() == DF_STEP_EVENT) {
    step();
    return 1;
  }

#ifdef NO_AI
  if (p_e->getType() == DF_KEYBOARD_EVENT) {
    EventKeyboard *p_keyboard_event = static_cast <EventKeyboard *> (p_e);

    LM.writeLog(10, "Hero::eventHandler(): key is '%c'", 
		p_keyboard_event->getKey());

    switch(p_keyboard_event->getKey()) {
    case '1':          // fire weapon 1
      if (ready(0))
	fire(0);
      break;
    case '2':          // fire weapon 2
      if (ready(1))
	fire(1);
      break;
    case '3':          // fire weapon 3
      if (ready(2))
	fire(2);
      break;
    case '4':          // fire weapon 4
      if (ready(3))
	fire(3);
      break;
    case '5':          // fire weapon 5
      if (ready(4))
	fire(4);
      break;
    case 'q':          // quit
      GaM.setGameOver();
      break;
    };;
  }
#endif

  // If get here, have ignored this event.
  return 0;
}

// Return true if Hero should shoot because will hit, else false.
bool Hero::aim(int gun) {

#ifdef NO_AI
  return true;
#endif

  // Get Saucer x location and speed;
  Saucer *p_s = getSaucer();
  int s_x = p_s->getPosition().getX();
  float s_speed = Config::getInstance()->getSpeedSaucer();

  // If Saucer past middle, don't shoot.
  int world_horizontal = Config::getInstance()->getWorldHorizontal();
  int world_vertical = Config::getInstance()->getWorldVertical();
  if ((s_x < world_horizontal/2 && p_s->getXVelocity() < 0) || // left
      (s_x > world_horizontal/2 && p_s->getXVelocity() > 0))   // right
    return false;

  // Compute Bullet stats.
  int b_distance = world_vertical - 9;		     // Spaces to top
  // Steps to top
  int b_steps = (int) ((float) b_distance / weapon[gun].getSpeed());

  // Compute Saucer stats.
  int s_distance = abs(world_horizontal/2 - s_x);    // Spaces to middle
  // Steps to middle
  int s_steps = (int) ((float) s_distance / s_speed);  

  LM.writeLog(8, "Hero::aim(): b_steps %d, s_steps %d", b_steps, s_steps);

  // See if steps for bullet and saucer is same, +1 or -1.
  if ((s_steps-1/s_speed <= b_steps) && (b_steps <= s_steps+1/s_speed))
    return true;		// shoot
  else
    return false;		// don't shoot
}

// Return true if ready to fire, else false.
bool Hero::ready(int gun) {

  // In case player tries to use non-existant gun.
  if (gun >= weapon_count)
    return false;

  // Gun can only fire once per Saucer.
  if (weapon[gun].getSaucerId() == getSaucer()->getId())
    // Not ready.
    return false;

  // Ready.
  return true;
}

// Launch bullet.
void Hero::fire(int gun) {

  // Create bullet.
  Position p(getPosition().getX(), getPosition().getY()-1);
  Bullet *p_bullet = new Bullet(p, weapon[gun]);

  // Create node to put in queue.
  Node node(p_bullet);
  
  // Put node in queue.
  Queue *p_q = getQueue();
  p_q -> enqueue(node);

  // Set id so can't shoot again until next Saucer.
  weapon[gun].setSaucerId(getSaucer()->getId());

  // Update stats.
  Stats::getInstance() -> incBulletsFired(gun);
}

// Do this every step event.
void Hero::step() {

#ifndef NO_AI

  // In order to avoid the order in the config file influencing the
  // fire order, shuffle the weapons each time.
  int gun[weapon_count];
  for (int i=0; i<weapon_count; i++)    // setup initial gun
    gun[i] = i;
  for (int i=weapon_count-1; i>0; i--) { // shuffle
    int j = random()%(i+1);
    // swap
    int temp = gun[i];
    gun[i] = gun[j];
    gun[j] = temp;
  }

  // Go through each weapon: ready? aim? fire!
  for (int i=0; i<weapon_count; i++)
    if (ready(gun[i]))
      if (aim(gun[i]))
	fire(gun[i]);

#else 

  for (int gun=0; gun<weapon_count; gun++)
    ready(gun);

#endif
}

// Utility to return "good" color based on number.
int getColor(int num) {
  switch(num) {
  case 0: return COLOR_CYAN; 
  case 1: return COLOR_YELLOW;
  case 2: return COLOR_MAGENTA;
  case 3: return COLOR_RED; 
  default: return COLOR_WHITE;
  }
}

// Add Weapon to Hero.
void Hero::addWeapon(Weapon weapon) {

  // See if room.
  if (weapon_count == MAX_WEAPONS) {
    LM.writeLog("Hero::addWeapon(): Unable to add weapon!  Max is %d.", 
		MAX_WEAPONS);
    return;
  }

  // Set color based on count.
  weapon.setColor(getColor(weapon_count));

  // Add weapon.
  this->weapon[weapon_count] = weapon;

  // Log weapon configuration parameters.
  LM.writeLog("Weapon %d", weapon_count);
  LM.writeLog("\taoe is %d", weapon.getAoE());
  LM.writeLog("\tspeed_bullet is %.2f", weapon.getSpeed());
  LM.writeLog("\tdamage is %d", weapon.getDamage());

  // Add to config.
  Config::getInstance() -> addWeapon(weapon, weapon_count);

  weapon_count++;
}
