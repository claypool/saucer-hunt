//
// Queue.cpp
// 

// Engine includes.
#include "EventStep.h"
#include "GameManager.h"

// Game includes.
#include "Config.h"
#include "game.h"
#include "Queue.h"
#include "util.h"

// Constructor.
Queue::Queue() {
  count = 0;
  setType("Queue");
  registerInterest(DF_STEP_EVENT);
}

// Add node to queue in right spot.
// + Must be at least lag time later.
// + Cannot have any two bullet events closer than gap time apart.
// Deactivate bullet.
void Queue::enqueue(Node node) {
  const int gap = Config::getInstance()->getGap();
  const int lag = Config::getInstance()->getLag();
  const int now = GaM.getStepCount();

  node.setEnqTime(now);
  LM.writeLog(5, "Queue::enqueue(): Enq bullet weapon %d, time %d, bullet %p", 
	      node.getBullet()->getWeapon().getId(), 
	      node.getEnqTime(), node.getBullet());

  // If list is full, can't add.
  if (count == MAX_QUEUE) {
    LM.writeLog("Queue::enqueue(): unable to add node. Full (%d)!",
		MAX_QUEUE);
    return;
  }

  // De-activate bullet (bullet is re-activated upon dequeue).
  node.getBullet()->setActive(false);

  // Get minimum queue time and expected impact at that time.
  int min_time = now + lag;
  LM.writeLog(5, "            min queue time is %d", min_time);

#ifndef NO_PRIORITY
  // See where queue time fits in.
  int i;
  for (i=0; i<count; i++) {
    if (min_time <= queue[i].getTime())
      break;
  }
  LM.writeLog(5, "              --> initial fit at spot %d", i);

  // Go through queue, figuring out greatest impact.
  for (int j=i; j<count; j++) {

    LM.writeLog(5, "               time_here %.d", queue[i].getTime());
    LM.writeLog(5, "              time_there %.d", queue[i].getTime()+gap);

    // Get impact of node and next node in queue.
    Weapon weapon = node.getBullet()->getWeapon();
    Weapon weapon_other = queue[j].getBullet()->getWeapon();
    LM.writeLog(5, "        impact_here_time %d", 
		queue[i].getTime() - node.getEnqTime());
    LM.writeLog(5, "  damage %d, speed %.2f, aoe %d", 
		weapon.getDamage(), weapon.getSpeed(), weapon.getAoE());
    float impact_here = computeExpectedImpact(
   	      queue[i].getTime() - node.getEnqTime(), 
	      weapon.getDamage(), 
	      weapon.getSpeed(), 
	      weapon.getAoE());
    float impact_other_there = computeExpectedImpact(
 	      queue[i].getTime()+gap - queue[i].getEnqTime(),
	      weapon_other.getDamage(), 
	      weapon_other.getSpeed(), 
	      weapon_other.getAoE());
    float impact_there = computeExpectedImpact(
              queue[i].getTime()+gap - node.getEnqTime(), 
	      weapon.getDamage(), 
	      weapon.getSpeed(), 
	      weapon.getAoE());
    float impact_other_here = queue[i].getImpact();

    // See if total higher impact here, versus next slot over (there)
    float total_here = impact_here + impact_other_there;
    float total_there = impact_there + impact_other_here;

    LM.writeLog(5, "        impact_here %.2f", impact_here);
    LM.writeLog(5, "       impact_there %.2f", impact_there);
    LM.writeLog(5, "  impact_other_here %.2f", impact_other_here);
    LM.writeLog(5, " impact_other_there %.2f", impact_other_there);
    LM.writeLog(5, "              TOTAL here %.2f", total_here);
    LM.writeLog(5, "             TOTAL there %.2f", total_there);

    // If so, put node here.
    if (total_here > total_there) {
      LM.writeLog(5, "           * weapon %d, higher impact fit at %d", 
		  weapon.getId(),
		  j);

      // Put this node here, then replace this node with others.
      node.setTime(queue[j].getTime());	// node time is node replacing

      // Pull out node being displaced.
      Node temp_node = queue[j];

      // Insert new node.
      node.setImpact(impact_here);
      queue[j] = node;

      // Repeat process, but for displaced node.
      node = temp_node;
    }
  }
#endif // FCFS/FIFO queue

  // Add last item to queue.
  LM.writeLog(5, "              --> adding bullet weapon %d to end of queue.",
	      node.getBullet()->getWeapon().getId());

  // Compute time.
  if (count == 0)  // queue empty?
    node.setTime(min_time);  // then node time is minimum
  else {
    int allowed_time  = queue[count-1].getTime() + gap;
    if (min_time < allowed_time)
      node.setTime(allowed_time);  // then node time is previous + gap
    else
      node.setTime(min_time);	// node time is minumum
  }

  // Compute impact.
  Weapon weapon = node.getBullet()->getWeapon();
  float impact = computeExpectedImpact(
		       node.getTime()-node.getEnqTime(), 
		       weapon.getDamage(), 
		       weapon.getSpeed(), 
		       weapon.getAoE());
  node.setImpact(impact);

  // Insert.
  LM.writeLog(5, "    new queue count is: %d", count);
  queue[count] = node;

  // Increase count.
  count++;
}

// Handle event.  
// If step, check time. 
// If time, dequeue first node and activate bullet.
int Queue::eventHandler(Event *p_e) {

  if (p_e->getType() == DF_STEP_EVENT) {
    EventStep *p_se = static_cast <EventStep *> (p_e);

    // If queue empty, then done.
    if (count==0) 
      return 1;

    // Time to dequeue first item?
    LM.writeLog(10, "Queue::eventHandler(): step count is %d", 
		p_se->getStepCount());
    LM.writeLog(10, "                       first item is %d",
		queue[0].getTime());

    while (queue[0].getTime() == p_se->getStepCount() && count > 0) {
      
      // Activate bullet.
      Bullet *p_b = queue[0].getBullet();
      p_b->setActive(true);

      LM.writeLog(5, "Queue:eventHandler(): DQ bullet %d, total delay %d", 
		  p_b->getWeapon().getId(), 
		  p_se->getStepCount() - queue[0].getEnqTime());
      
      // Remove item by scooting over.
      for (int i=0; i<count-1; i++)
	queue[i] = queue[i+1];
      count--;
    }

    return 1;

  }

  return 0;
}

// Print queue to log (for debugging).
void Queue::printQueue() {
  LM.writeLog("Queue::printQueue(): count is %d", count);
  for (int i=0; i<count; i++)
    LM.writeLog("\t%d: time %d, enq_time %d, impact_time %d, weapon %d, impact %.2f", 
		i, 
		queue[i].getTime(),
		queue[i].getEnqTime(),
		queue[i].getTime()-queue[i].getEnqTime(),
		queue[i].getBullet()->getWeapon().getId(),
		queue[i].getImpact());
}
