//
// Timer.h
//

#include "Event.h"		
#include "Object.h"

class Timer : public Object {

 private:
  int time_to_live;

 public:
  Timer(int time_to_live);
  int eventHandler(Event *p_e);
};
