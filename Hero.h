// Hero.h
//

// Engine includes.
#include "EventKeyboard.h"
#include "Object.h"

// Game includes.
#include "game.h"
#include "Weapon.h"

class Hero : public Object {

 private:
  int fire_countdown;
  int lag_countdown;
  Weapon weapon[MAX_WEAPONS];
  int weapon_count;

  void step();
  bool ready(int gun);
  bool aim(int gun);
  void fire(int gun);

 public:
  Hero();
  void addWeapon(Weapon weapon);
  int eventHandler(Event *p_e);
};
