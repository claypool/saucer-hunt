// 
// Weapon.cpp
// 

// Engine includes.
#include "GraphicsManager.h"	// for DF_COLOR_DEFAULT

// Game includes.
#include "game.h"
#include "Weapon.h"

void Weapon::construct() {
  color = DF_COLOR_DEFAULT;
  saucer_id = -1;
}

Weapon::Weapon() {
  construct();
  speed = SPEED;
  aoe = AOE;
  damage = DAMAGE;
}

Weapon::Weapon(int damage, int aoe, float speed) {
  construct();
  this->speed = speed;
  this->aoe = aoe;
  this->damage = damage;
}

void Weapon::setColor(int color) {
  this->color = color;
}

int Weapon::getColor() const {
  return color;
}

void Weapon::setSpeed(float speed) {
  this->speed = speed;
}

float Weapon::getSpeed() const {
  return speed;
}

void Weapon::setAoE(float aoe) {
  this->aoe = aoe;
}

int Weapon::getAoE() const {
  return aoe;
}

void Weapon::setDamage(int damage) {
  this->damage = damage;
}

int Weapon::getDamage() const {
  return damage;
}

int Weapon::getId() const {
  return id;
}

void Weapon::setId(int id) {
  this->id = id;
}

int Weapon::getSaucerId() const {
  return saucer_id;
}

void Weapon::setSaucerId(int saucer_id) {
  this->saucer_id = saucer_id;
}
