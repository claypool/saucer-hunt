//
// Explosion.cpp
//

// Engine includes.
#include "EventStep.h"
#include "GameManager.h"
#include "LogManager.h"
#include "ResourceManager.h"
#include "WorldManager.h"

// Game includes.
#include "Explosion.h"
#include "game.h"

Explosion::Explosion() {
  registerInterest(DF_STEP_EVENT);

#ifndef HEADLESS
  // Link to "explosion" sprite.
  Sprite *p_temp_sprite = RM.getSprite("explosion");
  if (!p_temp_sprite) {
    LM.writeLog("Explosion::Explosion(): Warning! Sprite '%s' not found", 
		"explosion");
    return;
  }
  setSprite(p_temp_sprite);
  time_to_live =  getSprite()->getFrameCount();
#else
  time_to_live = 8;
#endif

  setType("Explosion");
  setAltitude(4);

  setSolidness(SPECTRAL);
}

// Handle event.
// Return 0 if ignored, else 1.
int Explosion::eventHandler(Event *p_e) {

  if (p_e->getType() == DF_STEP_EVENT) {

    // Count down until explosion finished.
    time_to_live--;
    if (time_to_live <= 0)
      WM.markForDelete(this);
    return 1;

  }

  // If get here, have ignored this event.
  return 0;
}
