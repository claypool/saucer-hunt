#
# Makefile for Saucer Hunt game using Dragonfly
#
# 'make depend' to generate new dependency list
# 'make clean' to remove all constructed files
# 'make' to build executable
#
# Variables of interest:
#   PLATFORM and LDFLAGS for development platform
#   ENG is location of Dragonfly engine
#   INCPATH is path to Dragonfly includes
#   D* are various compilation options (see below)
#

#D1= -DCONSTANT_SPEED       # uncomment for constant speed Saucers
#D2= -DNO_AI                # uncomment for player control
#D3= -DTEST_QUEUE           # uncomment to test priority queue
#D3= -DTEST_IMPACT          # uncomment to test expected impact computation
D4= -DNO_PRIORITY          # uncomment for FCFS/FIFO queue only
D5= -DHEADLESS             # uncomment for headless mode
#D6= -DEI_LOOKUP            # uncomment for expected impact lookup
D6= -DEI_LINEAR            # uncomment for expected impact linear 
#D6= -DEI_EXPONENTIAL	    # uncomment for expected impact exponential

#PLATFORM= -DCYGWIN	    # uncomment for Cygwin
#PLATFORM= -DMACOS  	    # uncomment for MacOS

LDFLAGS= -lncurses -lrt	   # uncomment Linux & Cygwin
#LDFLAGS= -lncurses 	    # uncomment for MacOS

#CFLAGS= -g                 # uncomment for gprof or gdb
CFLAGS= -O2 -Wall

CC= g++ 
INCPATH= ../../dragonfly		# path to engine includes
ENG= ../../dragonfly/libdragonfly.a	# dragonfly engine
GAMESRC= \
	AoE.cpp \
	Bullet.cpp \
	Config.cpp \
	Explosion.cpp \
	Hero.cpp \
	Node.cpp \
	Points.cpp \
	Queue.cpp \
	Saucer.cpp \
	Star.cpp \
	Stats.cpp \
	Target.cpp \
	TestQueue.cpp \
	Timer.cpp \
	Weapon.cpp \
	util.cpp
GAME= game.cpp
EXECUTABLE= game		
OBJECTS= $(GAMESRC:.cpp=.o)

all: $(EXECUTABLE) 

$(EXECUTABLE): $(ENG) $(OBJECTS) $(GAME) $(GAMESRC) Makefile
	$(CC) $(CFLAGS) $(D1) $(D2) $(D3) $(D4) $(D5) $(D6) $(GAME) $(OBJECTS) $(ENG) $(PLATFORM) -o $@ -I$(INCPATH) $(LDFLAGS) 

%.o: %.cpp Makefile
	$(CC) -c $(CFLAGS) $(D1) $(D2) $(D3) $(D4) $(D5) $(D6) -I$(INCPATH) $(PLATFORM) $< -o $@

clean:
	rm -rf $(OBJECTS) $(EXECUTABLE) core dragonfly.log Makefile.bak *~

very-clean: clean
	rm -f stats-*.txt

depend: 
	makedepend *.cpp 2> /dev/null

# DO NOT DELETE
