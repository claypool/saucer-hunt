//
// Node.cpp
//

// Engine includes.
#include "GameManager.h"

// Game includes.
#include "game.h"
#include "Node.h"

Node::Node() {
  time = -1;
  p_bullet = NULL;
}

Node::Node(Bullet *p_bullet) {
  this->p_bullet = p_bullet;
}

int Node::getTime() const {
  return time;
}

int Node::getEnqTime() const {
  return enq_time;
}

float Node::getImpact() const {
  return impact;
}

Bullet *Node::getBullet() const {
  return p_bullet;
}

void Node::setTime(int time) {
  this->time = time;
}

void Node::setEnqTime(int enq_time) {
  this->enq_time = enq_time;
}

void Node::setImpact(float impact) {
  this->impact = impact;
}



