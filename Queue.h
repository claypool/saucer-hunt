//
// Queue.h - priority queue (sorted by time) of bullet launch events.
//

#ifndef QUEUE_H
#define QUEUE_H

// Engine includes.
#include "Event.h"
#include "Object.h"

// Game includes.
#include "game.h"
#include "Node.h"

class Queue : public Object {

 private:
  Node queue[MAX_QUEUE];     // Priority queue.
  int count;		     // Number of elements in queue.

 public:
  Queue();

  // Handle event.  
  // If step, check time. 
  // If time, dequeue first node and activate bullet.
  int eventHandler(Event *p_e);

  // Add node to queue in prority (time) order.
  // Deactivate bullet.
  void enqueue(Node node);

  // Print queue to log (for debugging).
  void printQueue();
};

#endif
