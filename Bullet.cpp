//
// Bullet.cpp
//

// System includes.
#include <stdlib.h>		// for abs()

// Engine includes.
#include "EventOut.h"
#include "LogManager.h"
#include "ResourceManager.h"
#include "WorldManager.h"

// Game includes.
#include "AoE.h"
#include "Bullet.h"
#include "Explosion.h"
#include "game.h"
#include "Saucer.h"

Bullet::Bullet(Position hero_pos, Weapon weapon) {

  // Set main attributes.
  this->weapon = weapon;

#ifndef HEADLESS
  // Link to "bullet" sprite.
  ResourceManager &resource_manager = ResourceManager::getInstance();
  char name[20];
  sprintf(name, "bullet-%d", weapon.getColor());
  Sprite *p_temp_sprite = resource_manager.getSprite(name);
  if (!p_temp_sprite) {
    LM.writeLog("Bullet::Bullet(): Warning! Sprite '%s' not found", name);
  } else {
    setSprite(p_temp_sprite);
    setSpriteSlowdown(5);		
  }
#endif

  setType("Bullet");
  setYVelocity(-1 * weapon.getSpeed());
  setSolidness(SOFT);
  setAltitude(2);

  // Set starting location, based on hero's position passed in.
  Position pos(hero_pos.getX(), hero_pos.getY()-1);
  setPosition(pos);

  LM.writeLog(5, "Bullet::Bullet(): at (%d, %d) from Weapon %d", 
	      pos.getX(), pos.getY(), weapon.getId());
}

// Handle event.
// Return 0 if ignored, else 1.
int Bullet::eventHandler(Event *p_e) {

  if (p_e->getType() == DF_OUT_EVENT) {
    out();
    return 1;
  }

  if (p_e->getType() == DF_COLLISION_EVENT) {
    EventCollision *p_collision_event = static_cast <EventCollision *> (p_e);
    hit(p_collision_event);
    return 1;
  }

  // If get here, have ignored this event.
  return 0;
}

// If bullet moves outside world, mark self for deletion.
void Bullet::out() {
  WM.markForDelete(this);
}

// If bullet hits Target, mark self for deletion.
void Bullet::hit(EventCollision *p_c) {

  LM.writeLog(5, "Bullet hit (%s, %s)!", 
	      p_c -> getObject1() -> getType().c_str(),
	      p_c -> getObject2() -> getType().c_str());

  // Only collisions with Target do anything.
  if ((p_c -> getObject1() -> getType() == "Target") ||
      (p_c -> getObject2() -> getType() == "Target")) {

    // Delete self.
    WM.markForDelete(this);

    // Create AoE
    new AoE(weapon);
  }
}

Weapon Bullet::getWeapon() const {
  return weapon;
}
