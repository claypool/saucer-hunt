// 
// Config.cpp
// 

// Game includes.
#include "game.h"
#include "Config.h"

Config::Config() {
  lag = LAG;
  world_horizontal = WORLD_HORIZONTAL;
  world_vertical = WORLD_VERTICAL;
  speed_saucer = SPEED_SAUCER;		 
  frame_time = FRAME_TIME;
  game_time = GAME_TIME;
  weapon_count = 0;
}

// Return singleton instance.
Config *Config::getInstance() {
  static Config config;
  return &config;
}

void Config::setLag(int lag) {
  this->lag = lag;
}

void Config::setWorldHorizontal(int world_horizontal) {
  this->world_horizontal = world_horizontal;
}

void Config::setWorldVertical(int world_vertical) {
  this->world_vertical = world_vertical;
}

void Config::setSpeedSaucer(float speed_saucer) {
  this->speed_saucer = speed_saucer;
}

void Config::setFrameTime(int frame_time) {
  this->frame_time = frame_time;
}

void Config::setGameTime(int game_time) {
  this->game_time = game_time;
}

void Config::setGap(int gap) {
  this->gap = gap;
}

int Config::getLag() const {
  return lag;
}

int Config::getWorldHorizontal() const {
  return world_horizontal;
}
	 
int Config::getWorldVertical() const {
  return world_vertical;
}
		 
float Config::getSpeedSaucer() const {
  return speed_saucer;
}
		 
int Config::getFrameTime() const {
  return frame_time;
}
		 
int Config::getGameTime() const {
  return game_time;
}

int Config::getGap() const {
  return gap;
}

int Config::getWeaponCount() const {
  return weapon_count;
}
		 
void Config::addWeapon(Weapon weapon, int gun) {
  this->weapon[gun] = weapon;
}

Weapon Config::getWeapon(int gun) const {
  return weapon[gun];
}

void Config::setWeaponCount(int weapon_count) {
  this->weapon_count = weapon_count;
}
