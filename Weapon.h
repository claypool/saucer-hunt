//
// Weapon.h
// 
#ifndef WEAPON_H
#define WEAPON_H

class Weapon {

 private:
  int id;
  int color;
  float speed;
  int aoe;
  int damage;
  int fire_countdown;
  int saucer_id;

 public:
  void construct();
  Weapon();
  Weapon(int damage, int aoe, float speed);

  // Get attributes.
  int getColor() const;
  float getSpeed() const;
  int getAoE() const;
  int getDamage() const;
  int getId() const;
  int getSaucerId() const;

  // Set attributes.
  void setColor(int color);
  void setSpeed(float speed);
  void setAoE(float aoe);
  void setDamage(int damage);
  void setId(int id);
  void setSaucerId(int saucer_id);
};

#endif
