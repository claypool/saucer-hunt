//
// game.h - global definition.
// 
#ifndef GAME_H
#define GAME_H

#include "LogManager.h"
#include "ResourceManager.h"
#include "WorldManager.h"

// Defaults
#define VERSION 1.7
#define GAME_NAME "Saucer Hunt"
#define CONFIG_FILENAME "config.txt"
#define OUTPUT_FILENAME "stats"
#define MAX_WEAPONS 5
#define MAX_QUEUE 100
#define PRIORITY 100

// Defaults if not defined in config file.
#define LAG 30			// Time before action (in steps).
#define WORLD_HORIZONTAL 80     // Game world horizontal.
#define WORLD_VERTICAL 24       // Game world vertical.
#define FRAME_TIME 33		// Game loop rate (in ms).
#define SPEED_SAUCER 0.25       // Saucer speed slowdown.
#define GAME_TIME 2700		// Steps in game lifetime.
#define AOE 4			// Area of effect (in spaces).
#define SPEED 1.0		// Bullet speed slowdown.
#define DAMAGE 1		// Hit points of damage.

#define GaM GameManager::getInstance()
#define GM GraphicsManager::getInstance()
#define LM LogManager::getInstance()
#define IM InputManager::getInstance()
#define RM ResourceManager::getInstance()
#define WM WorldManager::getInstance()

#endif
