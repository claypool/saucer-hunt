//
// game.cpp
// 

// System includes.
#include <stdlib.h>		// for atoi(), atof()
#include <fstream>		// for ifstream

// Engine includes.
#include "GameManager.h"
#include "GraphicsManager.h"	// for colors
#include "InputManager.h"
#include "LogManager.h"
#include "ResourceManager.h"
#include "utility.h"
 
// Game includes.
#include "Config.h"
#include "game.h"
#include "Hero.h"
#include "Points.h"
#include "Saucer.h"
#include "Queue.h"
#include "Star.h"
#include "Stats.h"
#include "Target.h"
#include "TestQueue.h"
#include "Timer.h"
#include "util.h"

// Remove '\r' from line (if there - typical of Windows)
void discardCR(string &str);	// Defined in libdragonfly.a

// Function prototypes.
void loadSprites(void);
void populateWorld(void);
void loadGlobalConfig(string str);
void loadWeaponConfig(string str, Hero *p_hero);
string readConfig();
void writeOutput();
void logConfig();
 
////////////////////////////////////////
int main(int argc, char *argv[]) {

#ifdef TEST_QUEUE
  // Test queue.
  LM.setLogLevel(10);
  new TestQueue;
  GaM.setHeadless();
  GaM.startUp();
  GaM.run();
  GaM.shutDown();
  return 0;
#endif

#ifdef TEST_IMPACT
  // computeExpectedImpact(int lag, int damage, float speed, int aoe);
  printf("ei: %.2f\n", computeExpectedImpact(10, 1, 4.0, 10));
  printf("ei: %.2f\n", computeExpectedImpact(10, 2, .25, 5)); 
  printf("ei: %.2f\n", computeExpectedImpact(40, 2, .25, 5)); 
  return 1;
#endif

  // Set logging level (higher for more messages).
  LM.setLogLevel(0);

#ifdef HEADLESS
  // Make headless.
  GaM.setHeadless();
#endif

  // Start up game manager.
  if (GaM.startUp())  {
    LM.writeLog("Error starting game manager!");
    GaM.shutDown();
    return 0;
  }

  // Write version to file.
  LM.writeLog("%s - version %.1f", GAME_NAME, VERSION);

  // Set flush of logfile during development (when done, make false).
  LM.setFlush(true);

  // Load global configuration parameters.
  loadGlobalConfig(readConfig());

  // Log global configuration parameters.
  logConfig();

#ifndef HEADLESS
  // Load game resources.
  loadSprites();
#endif

  // Setup some objects.
  populateWorld();

  // Run game (this blocks until game loop is over).
  GaM.run(Config::getInstance()->getFrameTime());
 
  // Shut everything down.
  GaM.shutDown();

  // Write performance output.
  writeOutput();
}
 
// Load sprites.
void loadSprites(void) {
  RM.loadSprite("sprites/saucer-spr.txt", "saucer");
  RM.loadSprite("sprites/ship-spr.txt", "ship");
  RM.loadSprite("sprites/explosion-spr.txt", "explosion");
  RM.loadSprite("sprites/bullet-spr-1.txt", "bullet-1");
  RM.loadSprite("sprites/bullet-spr-3.txt", "bullet-3");
  RM.loadSprite("sprites/bullet-spr-5.txt", "bullet-5");
  RM.loadSprite("sprites/bullet-spr-6.txt", "bullet-6");
  RM.loadSprite("sprites/bullet-spr-7.txt", "bullet-7");
}
 
// Populate world with some objects.
void populateWorld(void) {

  // Points object, top right.
  new Points;
  
  // Spawn some Stars.
  for (int i=0; i<20; i++) 
    new Star;

  // Saucer to shoot.
  new Saucer;

  // Hero to shoot at them.
  Hero *p_hero = new Hero;

  // Load Hero's weapons.
  loadWeaponConfig(readConfig(), p_hero);

  // Invisible target bullets hit and explode against.
  new Target;

  // Timer to countdown game.
  new Timer(Config::getInstance()->getGameTime());

  // Queue to hold Bullet events.
  new Queue;
}

// Read config file into string.
string readConfig() {

  // Open file.
  std::ifstream file(CONFIG_FILENAME);
  if (!file.is_open())   {
    LM.writeLog("readConfig(): Cannot open file '%s'.", CONFIG_FILENAME);
    return "";
  }

  // Repeat until end of file, adding each line to string.
  string str;
  while (file.good()) {
    string line;
    getline(file, line);  // Read line from file.
    discardCR(line);
    str += line;
  }

  // Return file contents as single string.
  return str;
}

// Load global parameters from string.
void loadGlobalConfig(string str) {

  // Get ready for parsing.
  match(str, "");		// populate string
  string val;

  // lag
  val = match("", "lag");
  if (!val.empty()) {
    int i = atoi(val.c_str());
    Config::getInstance()->setLag(i);
  }

  // world_horizontal
  val = match("", "world_horizontal");
  if (!val.empty()) {
    int i = atoi(val.c_str());
    Config::getInstance()->setWorldHorizontal(i);
  }

  // world_vertical
  val = match("", "world_vertical");
  if (!val.empty()) {
    int i = atoi(val.c_str());
    Config::getInstance()->setWorldVertical(i);
  }

  // speed_saucer
  val = match("", "speed_saucer");
  if (!val.empty()) {
    float f = atof(val.c_str());
    Config::getInstance()->setSpeedSaucer(f);
  }

  // frame_time
  val = match("", "frame_time");
  if (!val.empty()) {
    int i = atoi(val.c_str());
    Config::getInstance()->setFrameTime(i);
  }

  // game_time
  val = match("", "game_time");
  if (!val.empty()) {
    int i = atoi(val.c_str());
    Config::getInstance()->setGameTime(i);
  }

  // gap
  val = match("", "gap");
  if (!val.empty()) {
    int i = atoi(val.c_str());
    Config::getInstance()->setGap(i);
  }

  // num_weapons
  val = match("", "num_weapons");
  if (!val.empty()) {
    int i = atoi(val.c_str());
    Config::getInstance()->setWeaponCount(i);
  }

  return;
}

// Load weapon parameters from string into Hero.
void loadWeaponConfig(string str, Hero *p_hero) {

  LM.writeLog("WEAPONS");

  for (int index=0; index < Config::getInstance()->getWeaponCount(); index++) {

    string val;
    char match_str[80];
    int aoe;
    float speed;
    int damage;

    // aoe
    sprintf(match_str, "aoe_%d", index);
    val = match("", match_str);
    if (!val.empty())
      aoe = atoi(val.c_str());
    else
      aoe = AOE;
    
    // speed_bullet
    sprintf(match_str, "speed_bullet_%d", index);
    val = match("", match_str);
    if (!val.empty())
      speed = atof(val.c_str());
    else
      speed = SPEED;

    // damage
    sprintf(match_str, "damage_%d", index);
    val = match("", match_str);
    if (!val.empty())
      damage = atof(val.c_str());
    else
      damage = DAMAGE;

    // add weapon to Hero
    Weapon weapon;
    weapon.setColor(COLOR_CYAN);
    weapon.setSpeed(speed);
    weapon.setAoE(aoe);
    weapon.setDamage(damage);
    weapon.setId(index);
    p_hero->addWeapon(weapon);

  } // end of loop through all weapons
}

// Write performance statistics to file.
void writeOutput() {

  // Open stats file.
  char filename[80];
  int ext = 1;
  sprintf(filename, "%s-%s.txt", 
	  OUTPUT_FILENAME, 
	  getTimeString());
  while (ext < 999) {  // If  name conflict, append exstenion
    if (access(filename, F_OK) == -1) // No conflict
      break;
    sprintf(filename, "%s-%s-%d.txt", 
	    OUTPUT_FILENAME, 
	    getTimeString(),
	    ext);  // append in case filename collision
  }
  FILE *p_f = fopen(filename, "w");
  if (!p_f) {
    fprintf(stderr, "writeOutput(): Unable to open file '%s'",filename);
    return;
  }
  
  // Write configuration parameters.
  fprintf(p_f, "                 CONFIGURATION\n");
#ifdef NO_PRIORITY
  fprintf(p_f, "                   Queue: fcfs/fifo\n");
#else
  fprintf(p_f, "                   Queue: priority\n");
#endif
#ifdef EI_LOOKUP
  fprintf(p_f, "         Expected Impact: lookup\n");
#elif EI_LINEAR
  fprintf(p_f, "         Expected Impact: linear\n");
#elif EI_EXPONENTIAL
  fprintf(p_f, "         Expected Impact: exponential\n");
#else
  fprintf(p_f, "         Expected Impact: undefined\n");
#endif
#ifdef NO_AI
  fprintf(p_f, "                      AI: off\n");
#else
  fprintf(p_f, "                      AI: on\n");
#endif
#ifdef CONSTANT_SPEED
  fprintf(p_f, "            Saucer speed: constant\n");
#else
  fprintf(p_f, "            Saucer speed: variable\n");
#endif
  fprintf(p_f, "                     lag: %d\n", 
	  Config::getInstance()->getLag());
  fprintf(p_f, "                     gap: %d\n", 
	  Config::getInstance()->getGap());
  fprintf(p_f, "              frame_time: %d\n", 
	  Config::getInstance()->getFrameTime());
  fprintf(p_f, "               game_time: %d\n", 
	  Config::getInstance()->getGameTime());
  fprintf(p_f, "        world_horizontal: %d\n", 
	  Config::getInstance()->getWorldHorizontal());
  fprintf(p_f, "          world_vertical: %d\n", 
	  Config::getInstance()->getWorldVertical());
  fprintf(p_f, "            speed_saucer: %.2f\n", 
	  Config::getInstance()->getSpeedSaucer());

  fprintf(p_f,"\n                    WEAPONS\n");
  for (int gun=0; gun < Config::getInstance()->getWeaponCount(); gun++) {
    Weapon weapon = Config::getInstance() -> getWeapon(gun);
    fprintf(p_f, "              Weapon %d\n", gun);
    fprintf(p_f, "                     aoe: %d\n", weapon.getAoE());
    fprintf(p_f, "                  damage: %d\n", weapon.getDamage());
    fprintf(p_f, "                   speed: %.2f\n", weapon.getSpeed());
  }

  // Write performance statistics.
  Stats *p_stats = Stats::getInstance();
  fprintf(p_f, "\n                  PERFORMANCE\n");
  fprintf(p_f, "                      points: %d\n", 
	  p_stats->getTotalPoints());
  fprintf(p_f, "           points per minute: %.0f\n", 
	  p_stats->getTotalPoints() / 
	  ((float) Config::getInstance()->getGameTime()/(30*60)));
  for (int gun=0; gun < Config::getInstance()->getWeaponCount(); gun++) {
    fprintf(p_f, "\n           weapon %d - points: %d\n", 
	    gun, p_stats->getPoints(gun));
    fprintf(p_f, "    weapon %d - bullets fired: %d\n", 
	    gun, p_stats->getBulletsFired(gun));
    fprintf(p_f, "      weapon %d - bullets hit: %d\n", 
	    gun, p_stats->getBulletsHit(gun));
    fprintf(p_f, "   weapon %d - bullets missed: %d\n", 
	    gun, p_stats->getBulletsFired(gun) - 
	    p_stats->getBulletsHit(gun));
    if (p_stats->getBulletsHit(gun) > 0)
      fprintf(p_f, "  weapon %d - bullet accuracy: %.2f\n", 
	      gun, (float) 
	      p_stats->getBulletsHit(gun)/p_stats->getBulletsFired(gun));
    else
      fprintf(p_f, "  weapon %d - bullet accuracy: %.2f\n", gun, 0.0);
  }
  
  // Close file.
  fprintf(p_f, "\n");
  fclose(p_f);
  
#ifndef HEADLESS
  // Clear screen and print filename.
  printf("\033[2J");
  printf("\033[0;0f");
  printf("Output: %s\n", filename);
#endif
}


// Log global configuration parameters.
void logConfig() {
  LM.writeLog("CONFIGURATION");
#ifdef NO_PRIORITY
  LM.writeLog("FCFS/FIFO queue");
#else
  LM.writeLog("Priority queue");
#endif
#ifdef EI_LOOKUP
  LM.writeLog("Expected Impact: lookup");
#elif EI_LINEAR
  LM.writeLog("Expected Impact: linear");
#elif EI_EXPONENTIAL
  LM.writeLog("Expected Impact: exponential");
#else
  LM.writeLog("Expected Impact: undefined");
#endif
#ifdef NO_AI
  LM.writeLog("No AI");
#else
  LM.writeLog("AI");
#endif
#ifdef CONSTANT_SPEED
  LM.writeLog("Constant Saucer speed");
#else
  LM.writeLog("Variable Saucer speed");
#endif
  LM.writeLog("lag is %d", Config::getInstance()->getLag());
  LM.writeLog("gap is %d", Config::getInstance()->getGap());
  LM.writeLog("world_horizontal is %d", 
	      Config::getInstance()->getWorldHorizontal());
  LM.writeLog("world_vertical is %d", 
	      Config::getInstance()->getWorldVertical());
  LM.writeLog("speed_saucer is %.2f", Config::getInstance()->getSpeedSaucer());
  LM.writeLog("frame_time is %d", Config::getInstance()->getFrameTime());
  LM.writeLog("game_time is %d", Config::getInstance()->getGameTime());
  LM.writeLog("num_weapons is %d", Config::getInstance()->getWeaponCount());
}
