//
// TestQueue.h - just for testing priority queue out.
//

// Engine includes.
#include "Event.h"
#include "Object.h"

// Game includes.
#include "Bullet.h"
#include "Node.h"
#include "Queue.h"
#include "Weapon.h"

class TestQueue : public Object {

 private:
  Weapon w1, w2, w3, w4;
  Queue *p_q;

 public:
  TestQueue();
  int eventHandler(Event *p_e);
};


