//
// Target.cpp
//

// Game includes.
#include "game.h"
#include "Target.h"

Target::Target() {

  setType("Target");
  setSolidness(SOFT);

  // Set location.
  Position pos(WORLD_HORIZONTAL/2, 4);
  setPosition(pos);
  LM.writeLog(10, "Target::Target(): at (%d, %d)", pos.getX(), pos.getY());
}

