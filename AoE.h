//
// AoE.h
// 

// Engine includes.
#include "Object.h"

// Game includes
#include "Weapon.h"

class AoE : public Object {

 private:
  int time_to_live;
  Weapon weapon;		// Weapon that fired it.

 public:
  AoE(Weapon weapon);
  int eventHandler(Event *p_e);
  void draw();
};
