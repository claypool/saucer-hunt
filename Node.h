//
// Node.h - bullet launch event for Queue.
// 

#ifndef NODE_H
#define NODE_H

#include "Bullet.h"

class Node {

 private:
  int time;			// Time bullet scheduled to launch.
  int enq_time;			// Time player fired bullet.
  float impact;			// Expected impact of bullet in node.
  Bullet *p_bullet;

 public:
  Node();
  Node(Bullet *p_bullet);

  // Get attributes.
  int getTime() const;
  int getEnqTime() const;
  float getImpact() const;
  Bullet *getBullet() const;

  // Set attributes.
  void setTime(int time);
  void setEnqTime(int enq_time);
  void setImpact(float impact);
};

#endif
