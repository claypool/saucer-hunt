//
// AoE.cpp
//

// System includes.
#include <stdlib.h> 		// for abs()

// Engine includes
#include "EventStep.h"
#include "EventView.h"

// Game includes.
#include "AoE.h"
#include "game.h"
#include "GraphicsManager.h"
#include "Points.h"
#include "Position.h"
#include "Saucer.h"
#include "Stats.h"
#include "util.h"

AoE::AoE(Weapon weapon) {

  // Set attributes.
  this->weapon = weapon;
  setSolidness(SPECTRAL);
  setType("AoE");
  registerInterest(DF_STEP_EVENT);
  time_to_live = 1;

  // Set location.
  Position pos(WORLD_HORIZONTAL/2, 4);
  setPosition(pos);

  LM.writeLog(8, "AoE::AoE: at (%d,%d)", pos.getX(), pos.getY());
}


// Handle event.
// Return 0 if ignored, else 1.
int AoE::eventHandler(Event *p_e) {

  if (p_e->getType() == DF_STEP_EVENT) {

    // Count down until finished.
    time_to_live--;
    if (time_to_live <= 0)
      WM.markForDelete(this);

  }

  // If get here, have ignored this event.
  return 0;
}

// Draw AoE box and see if hit Saucer.
void AoE::draw() {

  // Draw box.
  for (int y = -1; y<2; y++)
    for (int x=-1*weapon.getAoE(); x<=weapon.getAoE(); x++) {
      Position p(getPosition().getX() - x, getPosition().getY()+y);
      GM.drawCh(p, '.', weapon.getColor());
    }

  // If Saucer intersects AoE box, then hit.
  Position p(getPosition().getX()-weapon.getAoE(), getPosition().getY()-1);
  Box b(p, 1+2*weapon.getAoE(), 3);
  Saucer *p_s = getSaucer();
  if (boxIntersectsBox(b, getWorldBox(p_s))) {

    // Delete Saucer.
    LM.writeLog(1, "AoE::draw(): hit saucer");
    WM.markForDelete(p_s);

    // Adjust game stats.
    Stats::getInstance() -> incBulletsHit(weapon.getId());
    Stats::getInstance() -> incPoints(weapon.getId(), weapon.getDamage());

    // Send "view" event with points to interested ViewObjects.
    // Add points == damage.
    EventView ev(POINTS_STRING, weapon.getDamage(), true);
    WM.onEvent(&ev);

  } else {
    LM.writeLog(1, "AoE::draw(): miss saucer");
  }

}
