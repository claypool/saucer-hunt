//
// Stats.h - singleton to keep global statistics
//

#ifndef STATS_H
#define STATS_H

// Game includes.
#include "game.h"

class Stats {

 private:
  Stats();			  // Private since a singleton.
  Stats (Stats const&);		  // Don't allow copy.
  void operator=(Stats const&);   // Don't allow assignment.
  int bullets_fired[MAX_WEAPONS]; // Total bullets fired by weapon.
  int bullets_hit[MAX_WEAPONS];	  // Number of hits by weapon.
  int bullets_points[MAX_WEAPONS];// Number of hits by weapon.
  int points;			  // Total number of points.

 public:
  static Stats *getInstance();

  // Get attributes.
  int getBulletsFired(int gun) const;
  int getBulletsHit(int gun) const;
  int getPoints(int gun) const;
  int getTotalPoints() const;

  // Set attributes.
  void setBulletsFired(int gun, int bullets_fired);
  void setBulletsHit(int gun, int bullets_hit);
  void setPoints(int gun, int points);

  // Increment attributes.
  void incBulletsFired(int gun);
  void incBulletsHit(int gun);
  void incPoints(int gun, int delta);
};

#endif
