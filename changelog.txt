Saucer Hunt Changelog

v1.7 - Removed load of Sprites in headless mode.
       Cleaned up some of the log messages and log levels.
v1.6 - Removed clear screen when in "headless" mode.
       Added computeExpectedImpact() lookup, linear, and exponential.
       Created -D compilation options for the above 3 choices.
       Modified Queue to use expected impact instead of priority.
       Updated TestQueue to test new priority queue implementation.
v1.5 - Added weapon config information to stats file.  Required re-working
         of Config class.
       Append extension to stats file in case of name collision when short run.
       Modified -D options in Makefile.
       Added option for "headless" mode (requires Dragonfly v3.4+).
v1.4 - Fixed so stats file correctly reports queue config.
       Updated base defaults in game.h.
       Updated so have stats per weapon in the Stats class.
v1.3 - Added "gap" to the config reporting in the stats file.
v1.2 - Modified so weapons shoot only 1 Bullet per Saucer.
v1.1 - Added Weapon class.
       Replaced global variables with Config singleton instead.
       Renamed utility.h,cpp to util.h,cpp.
       Added priority Queue for Bullet events.
       Added TestQueue class for testing priority queue.
       Added Node class for Queue.
       Adjusted aim of AI to more squarely hit constant Saucer.
       Added damage attribute for each weapon (points == damage).
       NO_AI option allows players to fire multiple weapons with 1-5.
v1.0 - Initial release.
