//
// Timer.cpp
//

// Engine includes
#include "EventStep.h"
#include "GameManager.h"

// Game includes.
#include "game.h"
#include "Timer.h"

Timer::Timer(int time_to_live) {
  setSolidness(SPECTRAL);

  // Set object type.
  setType("Timer");

  // Set time to live.
  this->time_to_live = time_to_live;

  // Need to count down each step.
  registerInterest(DF_STEP_EVENT);
}

// Handle event.
// Return 0 if ignored, else 1.
int Timer::eventHandler(Event *p_e) {

  if (p_e->getType() == DF_STEP_EVENT) {
    time_to_live--;
    if (time_to_live == 0)
      GaM.setGameOver();
    return 1;
  }

  // If get here, have ignored this event.
  return 0;
}
