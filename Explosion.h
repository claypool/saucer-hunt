//
// Explosion.h
//

#include "Object.h"

class Explosion : public Object {

 private:
  int time_to_live;

 public:
  Explosion();
  int eventHandler(Event *p_e);
};
