#!/bin/tcsh -f

echo Begin at `date`

/bin/rm -rf linear.data >& /dev/null

echo "speed\taoe\tslope\t\ty-intercept\tcorrelation" >> linear.data

foreach speed (0.25 0.5 0.75 1 2 3 4)

  foreach aoe (0 1 2 3 4 5 6 7 8 9 10)

    echo -n "Speed: $speed\t"
    echo "AoE: $aoe"

    echo -n "$speed\t$aoe\t" >> linear.data

    echo -n `cat speed-$speed-aoe-$aoe.data | stats -xy -GP | tail -1 | sed s/\*x/\/g | column 0 2` >> linear.data

    echo -n "\t" >> linear.data

    cat speed-$speed-aoe-$aoe.data | stats -xy | grep correlation | column 1 >> linear.data

  end # lag loop

end # each weapon speed

echo End at `date`
