#!/bin/tcsh -f

echo Begin at `date`

/bin/rm -rf exponential.data >& /dev/null
/bin/rm -rf fit.log >& /dev/null

echo "speed\taoe\ta *\t\t  exp(b*x)\t\tRMSE" >> exponential.data

foreach speed (0.25 0.5 0.75 1 2 3 4)

  foreach aoe (0 1 2 3 4 5 6 7 8 9 10)

    echo -n "Speed: $speed\t"
    echo "AoE: $aoe"

    echo -n "$speed\t$aoe\t" >> exponential.data

    echo -n `gnuplot -e "FILENAME='speed-$speed-aoe-$aoe.data'" fit.gp | & tail -1` >> exponential.data

    echo -n "\t" >> exponential.data

    gnuplot -e "FILENAME='speed-$speed-aoe-$aoe.data'" fit.gp | & grep rms | column 7 >> exponential.data

  end # lag loop

end # each weapon speed

echo End at `date`
