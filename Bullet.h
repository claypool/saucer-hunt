//
// Bullet.h
//
#ifndef BULLET_H
#define BULLET_H

// Engine includes.
#include "EventCollision.h"
#include "Object.h"

// Game includes.
#include "Weapon.h"

class Bullet : public Object {

 private:
  Weapon weapon;		// Weapon that fired Bullet.
  void out();
  void hit(EventCollision *p_c);

 public:
  Bullet(Position hero_pos, Weapon weapon);
  Weapon getWeapon() const;
  int eventHandler(Event *p_e);
};

#endif

