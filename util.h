//
// util.h
// 

// Game includes.
#include "Queue.h"
#include "Saucer.h"

// Return pointer to Saucer, NULL if not found.
Saucer *getSaucer();

// Return pointer to Queue, NULL if not found.
Queue *getQueue();

// Compute expected impact at given lag based on Damage, Speed, and AoE.
float computeExpectedImpact(int lag, int damage, float speed, int aoe);
